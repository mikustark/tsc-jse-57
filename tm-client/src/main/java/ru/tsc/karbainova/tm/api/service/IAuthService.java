package ru.tsc.karbainova.tm.api.service;

public interface IAuthService {

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);

}
