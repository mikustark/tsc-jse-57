package ru.tsc.karbainova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

@Component
public class TaskListShowListener extends AbstractSystemListener {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tascks";
    }

    @Override
    @EventListener(condition = "@taskListShowListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        taskEndpoint.findAllTask(sessionService.getSession()).stream().forEach(o -> System.out.println(o.getName()));
        System.out.println("[OK]");
    }
}
