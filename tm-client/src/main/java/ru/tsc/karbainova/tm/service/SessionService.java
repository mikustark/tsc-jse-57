package ru.tsc.karbainova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

@Getter
@Setter
@Service
public class SessionService {
    @Nullable
    private SessionDTO session;
}
