package ru.tsc.karbainova.tm.component;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.ILogService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListener;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;
import ru.tsc.karbainova.tm.listener.AbstractSystemListener;

import java.util.Scanner;

@Getter
@Setter
@Component
public class Bootstrap {

    @Autowired
    private ILogService logService;
    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;
    @Nullable
    @Autowired
    private AbstractSystemListener[] systemListeners;

    public void start(@NonNull final String... args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        startInput();
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }
    public void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return;
        @Nullable String command = null;
        for (@NotNull final AbstractSystemListener listener : systemListeners) {
            if (arg.equals(listener.arg())) command = listener.name();
        }
        if (command == null) return;
        publisher.publishEvent(new ConsoleEvent(command));
    }

    public void startInput() {
        logService.debug("process start...");
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!"exit".equals(command)) {
            try {
                System.out.println("Enter command:");
                command = scanner.nextLine();
                executeCommand(command);
                logService.info("`" + command + "` command executed");
            } catch (Exception e) {
                logService.error(e);
                System.out.println();
            }
        }
    }

    public void executeCommand(@NonNull final String commandName) {
        if (commandName == null || commandName.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(commandName));
    }
}
