package ru.tsc.karbainova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.karbainova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.karbainova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.karbainova.tm.api.service.dto.IProjectToTaskService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;

import javax.persistence.EntityManager;
import java.util.List;

import static org.reflections.util.Utils.isEmpty;

@Service
public class ProjectToTaskService implements IProjectToTaskService {
    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public IProjectDTORepository getProjectRepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    @NotNull
    public ITaskDTORepository getTaskRepository() {
        return context.getBean(ITaskDTORepository.class);
    }


    @Override
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.bindTaskById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.unbindTaskById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<TaskDTO> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findTasksByUserIdProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }

    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeTasksByProjectId(id);
            projectRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();

        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        try {
            entityManager.getTransaction().begin();
            ProjectDTO projectDTO = projectRepository.findByName(userId, name);
            removeProjectById(projectDTO.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (isEmpty(userId)) throw new EmptyIdException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        try {
            entityManager.getTransaction().begin();
            ProjectDTO projectDTO = projectRepository.findByIndex(userId, index);
            removeProjectById(projectDTO.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
