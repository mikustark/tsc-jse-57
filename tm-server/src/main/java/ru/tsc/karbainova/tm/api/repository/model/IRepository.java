package ru.tsc.karbainova.tm.api.repository.model;

import ru.tsc.karbainova.tm.model.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {
    void add(E entity);

    void update(E entity);
}
