package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.dto.LogDto;

public interface ILoggerService {
    void writeLog(LogDto message);
}
